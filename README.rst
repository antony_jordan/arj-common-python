Readme
======

This package is a small collection of Python utilities that are useful for various projects. It contains wrappers around some
common modules that simplify their use in certain cases and other pieces of code that I find useful in my own projects.

This is generally a place for me to place any code that I think is useful for other projects; a common resource for all of my
Python projects if you will.

Version 0.0.1

This version is still largely in flux and should not be considered as stable for the time being.

Warning:
   This package is written from a Linux point of view for Linux systems. It has not been tested with Windows, Mac or any
   other system. There are linux specific parts of this package and they are not currently guarded. Meaning they will most
   likely crash out on other platforms. This is something I will address at a later stage but it is not currently important
   to me as I do not plan to use this on any other platforms.

How do I get set up?
--------------------

Installation
~~~~~~~~~~~~

Installation can be performed using the bundled setup.py script.

Simply run 'sudo setup.py install' to get up and running.
All dependencies will be downloaded and installed by distutils for you.

Dependencies
~~~~~~~~~~~~

Running:
   * pyalsaaudio
   * icalendar
   * twiggy

Documentation:
   * sphinx_rtd_theme
   * sphinxcontrib.plantuml

Testing:
   TBD.

Testing
~~~~~~~

TBD.

Documentation
~~~~~~~~~~~~~

To generate the documentation simply execute 'make docs' from the root of the repository. This will generate both HTML and PDF-LATEX output in docs/_build.

To generate just HTML output execute 'make docs-html'

To generate just PDF output execute 'make docs-pdf'

Lint and Metrics
----------------

Prospector is used to run a suite of lint checks against the code (with customisations to meet the guide lines in the below
section). In general it is not acceptable for lint errors to remain in the code and they should be resolved as soon as
possible.

Radon is also used to run cyclomatic complexity analysis on the code. This helps identify parts of the code that are in need
of further work or that are un maintainable.

Radon uses a letter based system to denote complexity. The following rules are applied to the results from Radon:
   - A - B: Code is acceptable and maintainable.
   - C: Code is maintainable but work should be done to try to bring it's level of complexity down if possible.
        Code at this level should be the exception not the rule and should be well commented to aid in maintenance.
   - D+: Code at these levels is unacceptable and will be rejected. Code at these levels of complexity usually cause more
         trouble than they are worth and are a nightmare to maintain.

Contribution guidelines
-----------------------

If you would like to contribute please fork the repository and raise a pull request for any changes.

PEP8 should be used as the style guide for the package with the following additional rules:
   * Rules listed here shall take precedent over those in PEP8 if there is a conflict.
   * Indent stages shall use four spaces, tabs will not be permitted.
   * The line length limit is expanded to 125 characters (25 above the PEP8 expanded limit).
   * Despite the above docstrings shall be limited to 72 characters.
   * Each method and class must end with a comment line '# End of <block name>', this aids in clarity when reading.
   * Strings shall use single quotes over double quotes unless the string contains single quotes; in this case it is cleaner
     to use double quotes rather than escape the single quotes within the string.
   * Google style docstrings shall be used as they are cleaner.
   * Code that makes use of GPL licensed source shall not be used as it is incompatible with my license.
     I would suggest that you start your own repository instead or use a more permissive license.
   * Any license text should appear at the head of the file so that the license is preserved even if the file is separated
     from the package. This also allows for mixed license source.
   * If in doubt follow the general style of the file.

Who do I talk to?
-----------------

Antony Jordan: antony.r.jordan@gmail.com

Feel free to contact me about any of the work presented in this repo.

Make sure to include "arj_common" in the subject line so that I can clearly see what the mail refers to.

Please remember that I have a day job and may not be able to address every mail instantly. Be patient I will always reply as
soon as time allows.