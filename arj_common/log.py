# ######################################################################################################################### #
# Copyright Antony Robert Jordan 2016, All rights reserved.                                                                 #
#                                                                                                                           #
# License is hereby granted to use this software and source freely without restriction for all non-commercial purposes.     #
#                                                                                                                           #
# You are free to modify, use and distribute this software in compiled or source form for non-commercial use so long as     #
# attribution is given and this notice is present in the redistributed package.                                             #
#                                                                                                                           #
# For the purposes of this license agreement commercial use shall be deemed as the following:                               #
#    * Sale of this software or source in any form whether in full or part.                                                 #
#    * Use as part of an internal process or project that plays a part in generating profit.                                #
#    * Use as part of a larger product being sold for profit whether in full or in part.                                    #
#    * Publication of this source in print media including magazines and books.                                             #
#    * Publication of this source online behind a pay-wall or a members only site that requires payment to access the       #
#      contents.                                                                                                            #
#                                                                                                                           #
# Note:                                                                                                                     #
#    Provision is allowed for this source to be published on sites that make use of advertising for profit so long          #
#    as the source is viewable without interaction with the adverts being required (i.e. A pop up blocking the              #
#    view of the content requiring the user to click to reveal the source would not be permitted.)                          #
#                                                                                                                           #
# To request a commercial license please contact Antony Jordan at antony.r.jordan@gmail.com stating your intended use;      #
# replies will be made within 30 days of receiving a request. During this period permission is given to use the source and  #
# software for evaluation purposes providing no profit is made from the use of the source and software until a license has  #
# been acquired or further provisions made during negotiation of a commercial license agreement.                            #
#                                                                                                                           #
# All changes made to the source and software shall be clearly noted with the author name and a description of the changes  #
# made at the head of the file.                                                                                             #
#                                                                                                                           #
# The author reserves the right to alter or rescind this license at any point without notice.                               #
# This software is produced and licensed in the United Kingdom and is subject to all UK and European laws.                  #
#                                                                                                                           #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE      #
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR     #
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR          #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          #
# ######################################################################################################################### #

"""
Wrapper around twiggy logging library.

Provides a common interface to logging functions where the underlying
implementation is free to change.

Attributes:
    _LOG           : The twiggy logging object to use to write the log.
    _LOG_TO_CONSOLE: Also write the log message to the console.

Todo:
    This module needs to be generalised to allow it to be made into a
    separate package.
"""

import os
import twiggy

import arj_common

_LOG = None
_LOG_TO_CONSOLE = False


def init_log(path: str, level: twiggy.levels.LogLevel=twiggy.levels.DEBUG, log_console=False):
    """
    Initilises the logging function.

    Creates a new logger object.

    Args:
        path       : The path to the log file to write to.
        level      : The level that the log should operate at.
        log_console: Whether to log to the console or not.
    """

    global _LOG
    global _LOG_TO_CONSOLE

    log_file = os.path.join(get_log_directory(), path)

    twiggy.quick_setup(level, file=log_file)
    _LOG = twiggy.log
    _LOG_TO_CONSOLE = log_console
# End of init_log


def _get_logger(log_name: str=None, fields: {}=None) -> twiggy.logger:
    """
    Gets a logger instance and adds fields to it if they are specified.

    Args:
        log_name: The name to give the log.
        fields  : The fields to add to the log.

    Returns:
        A twiggy logger with the specified name and fields.
    """

    log = None

    if _LOG:
        if log_name:
            log = _LOG.name(log_name)
        else:
            log = _LOG

    if log:
        if fields:
            return log.fields(**fields)
        else:
            return log
    else:
        return None
# End of get_logger


def _format_fields(fields: {}) -> str:
    """
    Formats a given dictionary into a string.

    Args:
        fields: The dictioanry of fields to convert to string.

    Returns:
        A string in the format of '{<fields.key>: <fields.value>, ...}'
    """

    fields_str = '{'

    for key, val in fields.items():
        pair = str(key) + ': ' + str(val) + ', '

        fields_str += pair

    fields_str = fields_str[:-2] + '}'

    return fields_str
# End of format_fields


def _print_log_message(message: str, level: str, log_name: str=None, fields: {}=None):
    """
    Prints a log message to the screen along with all its attributes.

    This is to be used as a last resort if no other logger is
    available.

    Args:
        message : The message to print.
        level   : The debug level as a string to print as part of the
                  message.
        log_name: The name of the logger.
        fields  : The fields to print to the console that support this
                  log message.
    """

    if not _LOG or _LOG_TO_CONSOLE:
        log_message = level + ': '

        if log_name:
            log_message += log_name + ' '

        if fields:
            log_message += _format_fields(fields) + ' '

        log_message += message

        print(log_message)
# End of legacy log


def debug(log_text: str, log_name: str=None, **kwargs):
    """
    Writes a debug message to the log.

    Args:
        log_text: The log message to write.
        log_name: The name of the log to write to.
        **kwargs: Any fields that should be logged along with the
                  message.
    """

    log = _get_logger(log_name=log_name, fields=kwargs)

    if log:
        log.debug(log_text)

    # Write to console as a backup.
    _print_log_message(log_text, 'DEBUG', log_name=log_name, fields=kwargs)
# End of debug


def info(log_text: str, log_name: str=None, **kwargs):
    """
    Writes an info message to the log.

    Args:
        log_text: The log message to write.
        log_name: The name of the log to write to.
        **kwargs: Any fields that should be logged along with the
                  message.
    """

    log = _get_logger(log_name=log_name, fields=kwargs)

    if log:
        log.info(log_text)

    # Write to console as a backup.
    _print_log_message(log_text, 'INFO', log_name=log_name, fields=kwargs)
# End of info


def notice(log_text: str, log_name: str=None, **kwargs):
    """
    Writes a notice message to the log.

    Args:
        log_text: The log message to write.
        log_name: The name of the log to write to.
        **kwargs: Any fields that should be logged along with the
                  message.
    """

    log = _get_logger(log_name=log_name, fields=kwargs)

    if log:
        log.notice(log_text)

    # Write to console as a backup.
    _print_log_message(log_text, 'NOTICE', log_name=log_name, fields=kwargs)
# End of notice


def warning(log_text: str, log_name: str=None, **kwargs):
    """
    Writes a warning message to the log.

    Args:
        log_text: The log message to write.
        log_name: The name of the log to write to.
        **kwargs: Any fields that should be logged along with the
                  message.
    """

    log = _get_logger(log_name=log_name, fields=kwargs)

    if log:
        log.warning(log_text)

    # Write to console as a backup.
    _print_log_message(log_text, 'WARNING', log_name=log_name, fields=kwargs)
# End of warning


def error(log_text: str, log_name: str=None, **kwargs):
    """
    Writes an error message to the log.

    Args:
        log_text: The log message to write.
        log_name: The name of the log to write to.
        **kwargs: Any fields that should be logged along with the
                  message.
    """

    log = _get_logger(log_name=log_name, fields=kwargs)

    if log:
        log.debug(log_text)

    # Write to console as a backup.
    _print_log_message(log_text, 'ERROR', log_name=log_name, fields=kwargs)
# End of ERROR


def critical(log_text: str, log_name: str=None, **kwargs):
    """
    Writes a critical message to the log.

    Args:
        log_text: The log message to write.
        log_name: The name of the log to write to.
        **kwargs: Any fields that should be logged along with the
                  message.
    """

    log = _get_logger(log_name=log_name, fields=kwargs)

    if log:
        log.critical(log_text)

    # Write to console as a backup.
    _print_log_message(log_text, 'CRITICAL', log_name=log_name, fields=kwargs)
# End of critical


arj_common.make_library()
