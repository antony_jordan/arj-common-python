# ######################################################################################################################### #
# Copyright Antony Robert Jordan 2016, All rights reserved.                                                                 #
#                                                                                                                           #
# License is hereby granted to use this software and source freely without restriction for all non-commercial purposes.     #
#                                                                                                                           #
# You are free to modify, use and distribute this software in compiled or source form for non-commercial use so long as     #
# attribution is given and this notice is present in the redistributed package.                                             #
#                                                                                                                           #
# For the purposes of this license agreement commercial use shall be deemed as the following:                               #
#    * Sale of this software or source in any form whether in full or part.                                                 #
#    * Use as part of an internal process or project that plays a part in generating profit.                                #
#    * Use as part of a larger product being sold for profit whether in full or in part.                                    #
#    * Publication of this source in print media including magazines and books.                                             #
#    * Publication of this source online behind a pay-wall or a members only site that requires payment to access the       #
#      contents.                                                                                                            #
#                                                                                                                           #
# Note:                                                                                                                     #
#    Provision is allowed for this source to be published on sites that make use of advertising for profit so long          #
#    as the source is viewable without interaction with the adverts being required (i.e. A pop up blocking the              #
#    view of the content requiring the user to click to reveal the source would not be permitted.)                          #
#                                                                                                                           #
# To request a commercial license please contact Antony Jordan at antony.r.jordan@gmail.com stating your intended use;      #
# replies will be made within 30 days of receiving a request. During this period permission is given to use the source and  #
# software for evaluation purposes providing no profit is made from the use of the source and software until a license has  #
# been acquired or further provisions made during negotiation of a commercial license agreement.                            #
#                                                                                                                           #
# All changes made to the source and software shall be clearly noted with the author name and a description of the changes  #
# made at the head of the file.                                                                                             #
#                                                                                                                           #
# The author reserves the right to alter or rescind this license at any point without notice.                               #
# This software is produced and licensed in the United Kingdom and is subject to all UK and European laws.                  #
#                                                                                                                           #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE      #
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR     #
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR          #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          #
# ######################################################################################################################### #

"""
System audio settings manipulation.

Allows the modification of settings for the system's audio devices.
"""


import alsaaudio

import arj_common
import arj_common.log as log


def set_volume(step: int) -> int:
    """
    Increases or decreases the system volume based on the value
    specified.

    The value specified can be negative or positive.

    If a positive value is passed then the volume is increased by the
    value specified. If a negative value is passed then the volume is
    decreased by the value specified.

    Kwargs:
        step (int): The volume level to set (-100 .. 100)
    """

    mixer = alsaaudio.Mixer(alsaaudio.mixers()[0])
    vol = mixer.getvolume()[0]

    log.notice('Setting volume', requested_volume=vol)

    vol += step

    if vol > 100:
        vol = 100
    elif vol < 0:
        vol = 0

    log.notice('Setting volume', new_volume=vol)

    mixer.setvolume(vol)

    return vol
# End of Set_Volume


def increase_volume():
    """ Increases the system volume by 5% """
    set_volume(5)
# End of Increase_Volume


def decrease_volume():
    """ Decreases the system volume by 5% """
    set_volume(-5)
# End of Decrease_Volume

arj_common.make_library()
