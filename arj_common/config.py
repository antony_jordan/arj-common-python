# ######################################################################################################################### #
# Copyright Antony Robert Jordan 2016, All rights reserved.                                                                 #
#                                                                                                                           #
# License is hereby granted to use this software and source freely without restriction for all non-commercial purposes.     #
#                                                                                                                           #
# You are free to modify, use and distribute this software in compiled or source form for non-commercial use so long as     #
# attribution is given and this notice is present in the redistributed package.                                             #
#                                                                                                                           #
# For the purposes of this license agreement commercial use shall be deemed as the following:                               #
#    * Sale of this software or source in any form whether in full or part.                                                 #
#    * Use as part of an internal process or project that plays a part in generating profit.                                #
#    * Use as part of a larger product being sold for profit whether in full or in part.                                    #
#    * Publication of this source in print media including magazines and books.                                             #
#    * Publication of this source online behind a pay-wall or a members only site that requires payment to access the       #
#      contents.                                                                                                            #
#                                                                                                                           #
# Note:                                                                                                                     #
#    Provision is allowed for this source to be published on sites that make use of advertising for profit so long          #
#    as the source is viewable without interaction with the adverts being required (i.e. A pop up blocking the              #
#    view of the content requiring the user to click to reveal the source would not be permitted.)                          #
#                                                                                                                           #
# To request a commercial license please contact Antony Jordan at antony.r.jordan@gmail.com stating your intended use;      #
# replies will be made within 30 days of receiving a request. During this period permission is given to use the source and  #
# software for evaluation purposes providing no profit is made from the use of the source and software until a license has  #
# been acquired or further provisions made during negotiation of a commercial license agreement.                            #
#                                                                                                                           #
# All changes made to the source and software shall be clearly noted with the author name and a description of the changes  #
# made at the head of the file.                                                                                             #
#                                                                                                                           #
# The author reserves the right to alter or rescind this license at any point without notice.                               #
# This software is produced and licensed in the United Kingdom and is subject to all UK and European laws.                  #
#                                                                                                                           #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE      #
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR     #
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR          #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          #
# ######################################################################################################################### #

"""
A wrapper script around configparser with some helper functions to make
reading INI format files simpler.
"""

import configparser
import os

import arj_common
from arj_common import log

_LOG_NAME = 'Config'


def parser_to_dicts(parser: configparser.ConfigParser) -> {str: {str: str}}:
    """
    Processes all of the sections of a configparser and returns a
    dictionary of dictionary sections with name value pairs from the
    config parser.

    Args:
        parser: The configparser to process sections from.

    Returns:
        A dictionary of dictionaries containing the sections and their
        key value pairs in the form {section: {key: value}}.
    """

    log.debug('Converting parser to dictionary...', log_name=_LOG_NAME)

    parser_dicts = {}

    for section in parser.sections():
        log.debug('Converting section...', log_name=_LOG_NAME, section=section)

        parser_dicts[section] = section_to_dict(parser, section)

    log.debug('Finished converting parser to dictionary.', log_name=_LOG_NAME, dictionary=parser_dicts)

    return parser_dicts
# End of parser_to_dicts


def section_to_dict(parser: configparser.ConfigParser, section: str) -> {str: str}:
    """
    Converts a named config parser section into a dictionary.

    Args:
        parser : The config parser read the section from.
        section: The name of the section to convert.

    Returns:
        A dictionary in the form: {section-item: item-value}
    """

    log.debug('Converting section to dictionary.', log_name=_LOG_NAME, section=section)

    section_dict = {}
    options = parser.options(section)

    for option in options:
        log.debug('Adding option to dictionary.', log_name=_LOG_NAME, option=option)

        section_dict[option.upper()] = parser.get(section, option)

    log.debug('Finished converting section to dictionary.', log_name=_LOG_NAME, section=section, dictionary=section_dict)

    return section_dict
# End of section_to_dict


def read(config_path: str) -> configparser.ConfigParser:
    """
    Reads a INI file into a configparser object.

    Files should be of the format:

        [Section]
        Item_A: Value_A
        Item_B: Value_B

    Args:
        config_path: The path to the config file to parse.

    Returns:
        An instance of a configparser where the config file has been
        read in.
    """

    log.debug('Reading config from file.', log_name=_LOG_NAME, file=config_path)

    if not os.path.isfile(config_path):
        raise Exception('No configuration file found!!!')

    parser = configparser.ConfigParser()
    parser.read(config_path)

    log.debug('Finished reading config from file.', log_name=_LOG_NAME, file=config_path)

    return parser
# End of read


def read_config(config_path: str) -> {str: {str: str}}:
    """
    Parses an INI file into a dictionary of sections with all parsed
    items in further sub dictionaries.

    The INI file:

        [Section]
        Item_A: Value_A
        Item_B: Value_B

    Would be parsed into the following dictionary:

        {Section: {Item_A: Value_A, Item_B: Value_B}}

    Args:
        config_path: The path to the config file to parse.

    Returns:
        A dictionary of dictionaries containing all parsed sections
        from the file and their associated key value pairs.
    """

    log.debug('Reading config into dictionary...', log_name=_LOG_NAME, file=config_path)

    parser = read(config_path)

    retval = parser_to_dicts(parser)

    log.debug('Finished reading config into dictionary...', log_name=_LOG_NAME, file=config_path, dictionary=retval)

    return retval
# End of read_config

arj_common.make_library()
