# ######################################################################################################################### #
# Copyright Antony Robert Jordan 2016, All rights reserved.                                                                 #
#                                                                                                                           #
# License is hereby granted to use this software and source freely without restriction for all non-commercial purposes.     #
#                                                                                                                           #
# You are free to modify, use and distribute this software in compiled or source form for non-commercial use so long as     #
# attribution is given and this notice is present in the redistributed package.                                             #
#                                                                                                                           #
# For the purposes of this license agreement commercial use shall be deemed as the following:                               #
#    * Sale of this software or source in any form whether in full or part.                                                 #
#    * Use as part of an internal process or project that plays a part in generating profit.                                #
#    * Use as part of a larger product being sold for profit whether in full or in part.                                    #
#    * Publication of this source in print media including magazines and books.                                             #
#    * Publication of this source online behind a pay-wall or a members only site that requires payment to access the       #
#      contents.                                                                                                            #
#                                                                                                                           #
# Note:                                                                                                                     #
#    Provision is allowed for this source to be published on sites that make use of advertising for profit so long          #
#    as the source is viewable without interaction with the adverts being required (i.e. A pop up blocking the              #
#    view of the content requiring the user to click to reveal the source would not be permitted.)                          #
#                                                                                                                           #
# To request a commercial license please contact Antony Jordan at antony.r.jordan@gmail.com stating your intended use;      #
# replies will be made within 30 days of receiving a request. During this period permission is given to use the source and  #
# software for evaluation purposes providing no profit is made from the use of the source and software until a license has  #
# been acquired or further provisions made during negotiation of a commercial license agreement.                            #
#                                                                                                                           #
# All changes made to the source and software shall be clearly noted with the author name and a description of the changes  #
# made at the head of the file.                                                                                             #
#                                                                                                                           #
# The author reserves the right to alter or rescind this license at any point without notice.                               #
# This software is produced and licensed in the United Kingdom and is subject to all UK and European laws.                  #
#                                                                                                                           #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE      #
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR     #
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR          #
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          #
# ######################################################################################################################### #

"""
This module provides methods for retrieving calendar events from a
remote ICalendar feed.
"""

import datetime
import urllib.request

from icalendar import Calendar, Event

import arj_common


def is_event_on_day(event: Event, date: datetime.datetime) -> bool:
    """
    Checks if a given event is on a given day.

    Args:
        event: The event to check.
        date : The date to check the event against.

    Returns:
        =====   ==========================
        Value   Meaning
        =====   ==========================
        True    Event is on given date.
        False   Event is not on given date.
        =====   ==========================
    """

    event_dt = event.get('dtstart').dt

    is_dt = isinstance(event_dt, datetime.datetime)

    is_on_day = False

    if is_dt:
        event_date = event_dt.date()

        is_on_day = event_date == date

    return is_on_day
# end of is_event_on_day


def is_event_today(event: Event) -> bool:
    """
    Checks if a given event is today.

    Calls is_event_on_day with a date parameter of
    datetime.date.today().

    Args:
        event: The event to check.

    Returns:
        =====   ==================
        Value   Meaning
        =====   ==================
        True    Event is today.
        False   Event is not today.
        =====   ==================
    """

    return is_event_on_day(event, datetime.date.today())
# end of is_event_today


def get_event_summary(event: Event) -> str:
    """
    Gets the summary text for a given event.

    Args:
        event: The event to retrieve the details from.

    Returns:
        A string containing the event's summary text.
    """

    return event.get('summary')
# end of get_event_summary


def get_event_description(event: Event) -> str:
    """
    Gets the description from a given event.

    Args:
        event: The event to retrieve the details from.

    Returns:
        A string containing the event's description text.
    """

    return event.get('description')
# end of get_event_summary


def get_event_start(event: Event) -> datetime.datetime:
    """
    Gets the start date and time for a given event.
    Args:
        event: The event to retrieve the details from.

    Returns:
        A datetime containing the event's start date and time.
    """

    return event.get('dtstart').dt
# end of get_event_start


def get_remote_calendar(address: str) -> Calendar:
    """
    Gets an ICal calendar from a given URL.

    Args:
        address: The URL of the calendar to retrieve.

    Returns:
        A calendar object containing all of the events for the calendar
        at the specified URL.
    """

    ical_http_request = urllib.request.urlopen(address)
    ical_data = ical_http_request.read()
    calendar = Calendar.from_ical(ical_data)

    return calendar
# end of get_remote_calendar


def get_events_on_day(calendar: Calendar, date: datetime.datetime) -> [Event]:
    """
    Gets a list of events from a calendar that all occur on the given
    date.

    Note:
        This method uses list comprehension to sift through the
        calendar and find events.

        The comprehension loops through all events in the calendar
        and returns a list of any events that pass the call to
        is_event_on_day.

    Args:
        calendar: The calendar to look for events in.
        date    : The date to match events against.

    Returns:
        A list of events that all occur on the given date.
    """

    return [e for e in calendar.walk('vevent') if is_event_on_day(e, date)]
# end of get_events_on_day


def get_todays_events(calendar: Calendar) -> [Event]:
    """
    Gets a list of events from a calendar that all occur on today's
    date.

    Note:
        This method calls out to get_events_on_day with a date value of
        datetime.sate.today() in order to do all of its work.

    Args:
        calendar: The calendar to look for events in.

    Returns:
        A list of events that all occur today's date..
    """

    return get_events_on_day(calendar, datetime.date.today())
# end of get_todays_events

arj_common.make_library()
